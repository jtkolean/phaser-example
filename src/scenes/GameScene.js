import Phaser from 'phaser'
import ScoreLabel from '../ui/ScoreLabel'
import BombSpawner from './BombSpawner'

const BOMB = 'bomb'
const GROUND = 'ground'
const PLAYER = 'dude'
const STAR = 'star'

export default class GameScene extends Phaser.Scene {

    constructor() {
        super('game-scene')

        this.bombSpawner = undefined
        this.cursors = undefined
        this.gameOver = undefined
        this.player = undefined
        this.scoreLabel = undefined
    }

    preload() {
        this.load.image('sky', 'assets/sky.png')
        this.load.image(GROUND, 'assets/platform.png')
        this.load.image(STAR, 'assets/star.png')
        this.load.image(BOMB, 'assets/bomb.png')

        this.load.spritesheet(PLAYER,
            'assets/dude.png',
            { frameWidth: 32, frameHeight: 48 })
    }

    create() {
        this.add.image(400, 300, 'sky')

        const platforms = this.physics.add.staticGroup()
        platforms.create(400, 568, GROUND).setScale(2).refreshBody()
        platforms.create(600, 400, GROUND)
        platforms.create(50, 250, GROUND)
        platforms.create(750, 220, GROUND)

        this.player = this.physics.add.sprite(100, 450, PLAYER)
        this.player.setBounce(0.2)
        this.player.setCollideWorldBounds(true)

        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers(PLAYER, { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1
        })

        this.anims.create({
            key: 'turn',
            frames: [{ key: PLAYER, frame: 4 }],
            frameRate: 20
        })

        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers(PLAYER, { start: 5, end: 8 }),
            frameRate: 10,
            repeat: -1
        })

        this.stars = this.physics.add.group({
            key: STAR,
            repeat: 11,
            setXY: { x: 12, y: 0, stepX: 70 }
        })

        this.stars.children.iterate((/** @type {Phaser.Physics.Arcade.Sprite} */ star) => {
            star.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8))
        })

        this.scoreLabel = new ScoreLabel(this, 16, 16, 0, { fontSize: '32px', fill: '#000' })
        this.add.existing(this.scoreLabel)

        this.bombSpawner = new BombSpawner(this, BOMB)
        this.bombs = this.bombSpawner.group

        this.cursors = this.input.keyboard.createCursorKeys()
        this.physics.add.collider(this.player, platforms)
        this.physics.add.collider(this.stars, platforms)
        this.physics.add.collider(this.bombs, platforms)
        this.physics.add.collider(this.player, this.bombs, this.hitBomb, null, this)
        this.physics.add.overlap(this.player, this.stars, this.collectStar, null, this)
    }

    /**
     * @param {Phaser.Physics.Arcade.Sprite} player 
     * @param {Phaser.Physics.Arcade.Sprite} star 
     */
    collectStar(player, star) {
        star.disableBody(true, true)
        this.scoreLabel.add(10)

        if (this.stars.countActive(true) === 0) {
            this.stars.children.iterate((/** @type {Phaser.Physics.Arcade.Sprite} */ star) => {
                star.enableBody(true, star.x, 0, true, true)
            })
        }

        this.bombSpawner.spawn(player.x)
    }

    /**
     * @param {Phaser.Physics.Arcade.Sprite} player 
     * @param {Phaser.Physics.Arcade.Sprite} bomb 
     */
    hitBomb(player, bomb) {
        this.physics.pause()
        player.setTint(0xff0000)
        player.anims.play('turn')
        this.gameOver = true
    }

    update() {
        if (this.gameOver) {
            return
        }

        if (this.cursors.left.isDown) {
            this.player.setVelocityX(-160)
            this.player.anims.play('left', true)
        }
        else if (this.cursors.right.isDown) {
            this.player.setVelocityX(180)
            this.player.anims.play('right', true)
        }
        else {
            this.player.setVelocityX(0)
            this.player.anims.play('turn')
        }

        if (this.cursors.up.isDown && this.player.body.touching.down) {
            this.player.setVelocityY(-330)
        }
    }
}